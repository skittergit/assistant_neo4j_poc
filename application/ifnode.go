package application

import (
	"fmt"
	"workativ.com/assistant_neo4j_poc/domain/ifnode"
)

type ParentNotFoundErr struct {
	parent string
}

func (p ParentNotFoundErr) Error() string {
	return fmt.Sprintf("%s is not a valid parent", p.parent)
}

/*
IfNodeApp - this is a representation of application layer of if node app, the web interface will interact with it.
*/
type IfNodeApp struct {
	Repo ifnode.Repo
}

//NewIfNodeApp - This will give you a new if node app.
func NewIfNodeApp(Repo ifnode.Repo) *IfNodeApp {
	return &IfNodeApp{Repo: Repo}
}

/*
Get - This method will return single if node.
if withDesc is true then, it will fetch all the descendant if nodes.
*/
func (app *IfNodeApp) Get(id string, load ifnode.LoadConfig) (*ifnode.IfNode, error) {
	return app.Repo.Get(id, load)
}

/*
Get - This method will return all if nodes.
if OnlyRoot is true then, it will fetch only root if nodes.
*/
func (app *IfNodeApp) GetAll(load ifnode.LoadAllConfig) ([]*ifnode.IfNode, error) {
	return app.Repo.GetAll(load)
}

/*
Save - This method will save ifnode to database.
*/
func (app *IfNodeApp) Save(node *ifnode.IfNode) (*ifnode.IfNode, error) {
	if node.Parent != "" {
		exists, err := app.Repo.Exists(node.Parent)
		if err != nil {
			return nil, err
		}
		if !exists {
			return nil, ParentNotFoundErr{parent: node.Parent}
		}
	}

	return app.Repo.Save(node)
}
