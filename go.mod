module workativ.com/assistant_neo4j_poc

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/google/uuid v1.1.1
	github.com/neo4j-drivers/gobolt v1.7.4 // indirect
	github.com/neo4j/neo4j-go-driver v1.7.4
)
