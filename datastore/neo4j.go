package datastore

import (
	"github.com/neo4j/neo4j-go-driver/neo4j"
)

var driver neo4j.Driver

const defaultURI = "bolt://localhost:7687"

const defaultUser = "neo4j"

const defaultPassword = "root@123"

// Neo4jdriver - it is a singledon function used to get connection pool of neo4j
func Neo4jdriver() (neo4j.Driver, error) {
	var err error
	if driver == nil {
		driver, err = neo4j.NewDriver(defaultURI, neo4j.BasicAuth(defaultUser, defaultPassword, ""))
		if err == nil {
			return driver, err
		}
		return nil, err
	}
	return driver, nil
}

// Session will be a just getter function to create db's session
// all transactions should created by session only.
func Session(accessMode neo4j.AccessMode) (neo4j.Session, error) {
	driver, err := Neo4jdriver()
	if err != nil {
		return nil, err
	}
	return driver.Session(accessMode)
}
