package ifnode

// IfNode - This structure used to represent a if node
type IfNode struct {
	ID       string    `json:"id"`
	Title    string    `json:"title"`
	Children []*IfNode `json:"children"`
	Parent   string    `json:"parent"`
}
