package ifnode

// LoadConfig - Definition for how to load a single if node. use this with Get method of repo
type LoadConfig struct {
	// withDesc - set true if you need to load ifNode with it's descendant if nodes.
	WithDesc bool
	// withResNodes - set true if you need to load ifNode with it's response nodes.
	WithResNodes bool
}

// LoadAllConfig - Definition for how to load all if node from db. use this with GetAll method of repo
type LoadAllConfig struct {
	OnlyRoot     bool
	WithResNodes bool
}

//Repo - this interface will be used as
type Repo interface {
	Exists(id string) (bool, error)
	Get(id string, withDesc LoadConfig) (*IfNode, error)
	GetAll(LoadAllConfig) ([]*IfNode, error)
	Save(*IfNode) (*IfNode, error)
}
