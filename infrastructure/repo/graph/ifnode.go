package graph

import (
	"fmt"
	"github.com/neo4j/neo4j-go-driver/neo4j"
	"workativ.com/assistant_neo4j_poc/datastore"
	ifnodeDomain "workativ.com/assistant_neo4j_poc/domain/ifnode"
)

/*
IfRepo - this is a implementation of if node repository interface.
this is a abstract structure to talk with neo4j regarding only if nodes.
*/
type IfRepo struct {
	driver neo4j.Driver
}

type notAValidNodeErr struct{}

func (n *notAValidNodeErr) Error() string {
	return fmt.Sprintf("Not a valid node")
}

type convertedIfNodeData struct {
	err    error
	ifNode *ifnodeDomain.IfNode
}

func convertIfNodesRoutine(nodes []map[string]interface{}) <-chan *convertedIfNodeData {
	convertedIfNodeChannel := make(chan *convertedIfNodeData)
	for _, node := range nodes {
		go func(node map[string]interface{}) {
			ifNode, err := convertIfNode(node)
			convertedIfNodeChannel <- &convertedIfNodeData{ifNode: ifNode, err: err}
		}(node)
	}
	return convertedIfNodeChannel
}

func convertIfNode(node map[string]interface{}) (*ifnodeDomain.IfNode, error) {
	if len(node) != 0 {
		idInterface, _ := node["id"]
		titleInterface, _ := node["title"]
		childOfInterface, _ := node["child_of"]
		childrenInterface, _ := childOfInterface.([]interface{})
		children := make([]map[string]interface{}, 0)
		for _, childInterface := range childrenInterface {
			child, _ := childInterface.(map[string]interface{})
			children = append(children, child)
		}
		id, _ := idInterface.(string)
		title, _ := titleInterface.(string)
		convertedChildren := []*ifnodeDomain.IfNode{}
		if len(children) > 0 {

			converterChannel := convertIfNodesRoutine(children)
			for i := 0; i < len(children); i++ {
				conData := <-converterChannel
				if conData.err != nil {
					return nil, conData.err
				}
				convertedChildren = append(convertedChildren, conData.ifNode)
			}
		}
		return &ifnodeDomain.IfNode{ID: id, Title: title, Children: convertedChildren}, nil
	}
	return nil, &notAValidNodeErr{}
}

func (repo *IfRepo) loadIfNodeWithDesc(id string, WithResNodes bool) (*ifnodeDomain.IfNode, error) {
	var session neo4j.Session
	var err error
	if session, err = repo.driver.Session(neo4j.AccessModeRead); err != nil {
		return nil, err
	}
	defer session.Close()
	query := "match (root:IF_NODE) where root.id=$uuid Optional match path=((root)<-[:CHILD_OF*0..]-(leaf:IF_NODE)) with collect(path) as paths Call apoc.convert.toTree(paths) yield value return value"
	record, err := neo4j.Single(session.ReadTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		return tx.Run(query, map[string]interface{}{"uuid": id})
	}))
	if err != nil {
		return nil, err
	}
	recordValue, _ := record.Get("value")
	_neoNode := recordValue.(map[string]interface{})

	return convertIfNode(_neoNode)
}

// Get - method implementation of ifnode.Repo interface
func (repo *IfRepo) Get(id string, loadConfig ifnodeDomain.LoadConfig) (*ifnodeDomain.IfNode, error) {

	if loadConfig.WithDesc {
		return repo.loadIfNodeWithDesc(id, loadConfig.WithResNodes)
	}
	panic("still not implemented")
	// return nil, nil
}

func (repo *IfRepo) loadRoots() ([]*ifnodeDomain.IfNode, error) {
	var session neo4j.Session
	var err error
	if session, err = repo.driver.Session(neo4j.AccessModeRead); err != nil {
		return nil, err
	}
	defer session.Close()
	record, err := session.ReadTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		result, err := tx.Run("match (n:IF_NODE) where not (n)-[:CHILD_OF]->() return n", nil)
		if err != nil {
			return nil, err
		}
		ifNodes := make([]*ifnodeDomain.IfNode, 0)
		for result.Next() {
			node, _ := result.Record().Get("n")
			propertyMap := node.(neo4j.Node).Props()
			title, _ := propertyMap["title"].(string)
			id, _ := propertyMap["id"].(string)
			ifNodes = append(ifNodes, &ifnodeDomain.IfNode{Title: title, ID: id})
		}
		return ifNodes, nil
	})
	return record.([]*ifnodeDomain.IfNode), nil
}

//GetAll - method implementation of ifnode.Repo interface
func (repo *IfRepo) GetAll(loadConfig ifnodeDomain.LoadAllConfig) ([]*ifnodeDomain.IfNode, error) {
	if loadConfig.OnlyRoot {
		return repo.loadRoots()
	}
	panic("still not implemented")
	// return nil, nil
}

/*
Exists - This method will accept node id and checks that is existed in db
*/
func (repo *IfRepo) Exists(id string) (bool, error) {
	var session neo4j.Session
	var err error

	if session, err = repo.driver.Session(neo4j.AccessModeWrite); err != nil {
		return false, err
	}
	defer session.Close()

	returned, err := session.ReadTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		return tx.Run(
			"Match (n:IF_NODE) where n.id=$uuid return n IS NOT null as parent_exists",
			map[string]interface{}{"uuid": id})
	})
	if err != nil {
		return false, err
	}
	result, _ := returned.(neo4j.Result)
	if result.Next() {
		record := result.Record()
		exists, ok := record.Get("parent_exists")
		if !ok {
			return false, nil
		}
		return exists.(bool), nil
	}
	return false, nil
}

// Save - method implementation of ifnode.Repo interface
func (repo *IfRepo) Save(node *ifnodeDomain.IfNode) (*ifnodeDomain.IfNode, error) {
	var session neo4j.Session
	var err error

	if session, err = repo.driver.Session(neo4j.AccessModeWrite); err != nil {
		return nil, err
	}
	defer session.Close()

	_, err = session.WriteTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		var result neo4j.Result
		query := "create (:NODE:IF_NODE {title: $title, id: $uuid})"
		if node.Parent != "" {
			query = "match (parent:IF_NODE:NODE) where parent.id=$parent create (parent)<-[:CHILD_OF]-(:NODE:IF_NODE {title: $title, id: $uuid})"
		}
		if result, err = tx.Run(query,
			map[string]interface{}{
				"title":  node.Title,
				"uuid":   node.ID,
				"parent": node.Parent,
			}); err != nil {
			return nil, err
		}
		return result.Consume()
	})

	if err != nil {
		return nil, err
	}
	return node, nil
}

/*
NewIfRepo - this will create a new repo for ifNode
*/
func NewIfRepo() (*IfRepo, error) {
	var driver neo4j.Driver
	var err error
	driver, err = datastore.Neo4jdriver()
	if err == nil {
		return &IfRepo{driver: driver}, nil
	}
	return nil, err
}
