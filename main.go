package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"workativ.com/assistant_neo4j_poc/ifnode"
)

// DefaultPort will be used if PORT not found in env
const defaultPort = "3030"

func port() string {
	PORT := os.Getenv("PORT")
	if PORT == "" {
		return defaultPort
	}
	return PORT
}

func main() {
	router := gin.Default()
	router.GET("/live", func(c *gin.Context) {
		c.String(http.StatusOK, "I'm working")
	})
	ifnode.InitHandler(router)
	router.Run(":" + port())
}
