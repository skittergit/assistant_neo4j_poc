package ifnode

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"workativ.com/assistant_neo4j_poc/application"
	ifNodeDomain "workativ.com/assistant_neo4j_poc/domain/ifnode"
	"workativ.com/assistant_neo4j_poc/infrastructure/repo/graph"
)

const (
	ParentNotFound = 0
)

// Handler - this struct will be used to
type Handler struct {
	router *gin.Engine
	app    *application.IfNodeApp
}

func (h *Handler) create(c *gin.Context) {
	ifnodeToCreate := &ToCreate{}

	if err := c.ShouldBindJSON(ifnodeToCreate); err == nil {
		ifNode, err := ifnodeToCreate.toIfNode()
		if err == nil {
			_, err = h.app.Save(ifNode)
		}

		switch err.(type) {
		case application.ParentNotFoundErr:
			c.JSON(http.StatusBadRequest,
				map[string]interface{}{"code": ParentNotFound, "message": err.Error()})
		case nil:
			c.JSON(http.StatusOK, ifNode)
		default:
			fmt.Println("Internal Server Error ::: ", err)
			c.String(http.StatusInternalServerError, "Internal Server Error. Please try again after sometime")
		}

	} else {
		c.String(http.StatusBadRequest, "Please check the parameters and try again")
	}
}

func (h *Handler) getAll(c *gin.Context) {
	onlyRootParam, err := strconv.ParseBool(c.DefaultQuery("root", "true"))
	if err != nil {
		c.String(http.StatusBadRequest, "Please use true or false for the query param root")
		return
	}

	ifNodes, err := h.app.GetAll(ifNodeDomain.LoadAllConfig{OnlyRoot: onlyRootParam})

	switch err.(type) {
	case nil:
		c.JSON(http.StatusOK, ifNodes)
	default:
		fmt.Println("Internal Server Error ::: ", err)
		c.String(http.StatusInternalServerError, "Internal Server Error. Please try again after sometime")
	}
}

func (h *Handler) get(c *gin.Context) {
	id := c.Param("id")
	ifNode, err := h.app.Get(id, ifNodeDomain.LoadConfig{WithDesc: true})
	switch err.(type) {
	case nil:
		c.JSON(http.StatusOK, ifNode)
	default:
		fmt.Println("Internal Server Error ::: ", err)
		c.String(http.StatusInternalServerError, "Internal Server Error. Please try again after sometime")
	}
}

func (h *Handler) registerRoutes() {
	h.router.POST("/if_node", h.create)
	h.router.GET("/if_nodes", h.getAll)
	h.router.GET("/if_node/:id", h.get)
}

// InitHandler - will initiates handler with initial configuration like route registering
func InitHandler(router *gin.Engine) *Handler {
	repo, err := graph.NewIfRepo()
	if err != nil {
		panic(err)
	}
	handler := &Handler{router: router, app: application.NewIfNodeApp(repo)}
	handler.registerRoutes()
	return handler
}
