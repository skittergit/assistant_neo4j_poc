package ifnode

import (
	"github.com/google/uuid"
	"github.com/neo4j/neo4j-go-driver/neo4j"
	"workativ.com/assistant_neo4j_poc/datastore"
	ifnodeDomain "workativ.com/assistant_neo4j_poc/domain/ifnode"
)

//ToCreate - this will used as a model to receive create if node
type ToCreate struct {
	Parent string `json:"parent"`
	Title  string `json:"title" binding:"required"`
}

func (tc *ToCreate) haveParent() bool {
	return tc.Parent != ""
}

func (tc *ToCreate) toIfNode() (*ifnodeDomain.IfNode, error) {
	var randomUUID uuid.UUID
	var err error
	if randomUUID, err = uuid.NewRandom(); err != nil {
		return nil, err
	}
	return &ifnodeDomain.IfNode{ID: randomUUID.String(), Title: tc.Title, Parent: tc.Parent}, nil
}

func (tc *ToCreate) save() error {
	var session neo4j.Session
	var err error
	var randomUUID uuid.UUID
	if session, err = datastore.Session(neo4j.AccessModeWrite); err != nil {
		return err
	}
	defer session.Close()

	if randomUUID, err = uuid.NewRandom(); err != nil {
		return err
	}

	_, err = session.WriteTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		var result neo4j.Result
		if result, err = tx.Run("create (:NODE:IF_NODE {title: $title, id: $uuid})", map[string]interface{}{"title": tc.Title, "uuid": randomUUID.String()}); err != nil {
			return nil, err
		}
		return result.Consume()
	})
	if err != nil {
		return err
	}
	return nil
}
